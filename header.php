<?php
session_start();

require_once "webFunctions.php";
?>
<html>
<body style="font-family: Book Antiqua;background:#FF1493">
<h1 id="header" style = "font-size:65px"><a href="homePage.php"><img src="M.jpg" alt="M.jpg" width = "55" height = "55" style = "border:1px solid black"/></a> Muskie Library Attendance</h1>
<?php
if(isset($_SESSION['key']))
{
    ?>
	<a href = "logout.php" style = "color: black" > Sign Out </a> |
	<a href = "displayData.php" style = "color: black"> View Attendance </a>
	<?php
	$user = getUser($pdo, $_SESSION['key']);
	
	if ($user['isAdmin'])
	{
?>
	| <a href = "admin.php" style = "color: black"> Admin Area </a>
<?php		
	}
}
else
{
	// Creating accounts must be part of the admin section
	// <a href = "createAccountHtml.php" style = "color: black"> Create Account </a>
    ?>
  	
  	|
  	<a href = "main.php" style = "color: black"> Sign In </a>
 <?php 
}
?>
<hr style="height:1px;color:black;background-color:black;"/>