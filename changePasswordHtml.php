<?php
include 'header.php';

$pageTitle = "Change Password";
if(isset($_SESSION['error']))
{
    echo '<h2>'.$_SESSION['error'].'</h2>';
    unset($_SESSION['error']);
}
?>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" /> <input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Back to Admin Controls" onclick="window.location.href='admin.php'"<br><br>
<h1> Change Password: </h1>
	<form id='changePassword' action='changePassword.php' method='post' accept-charset='UTF-8'>
		<fieldset>
			<legend id = "change">Change Password</legend>
			<label for='userID'> User Name: </label> 
			<br>
			<input type='text' name='userName' id='userName' maxlength="100" /> 
			<br>
			<label for='password'> Current Password: </label> 
			<br>
			<input type='password' name='password' id='password' maxlength="50" /> 
			<br>
			<label for='confirmPassword'> Confirm Current Password:</label>
			<br>
			<input type='password' name='confirmPassword' id='confirmPassword' maxlength="50" /> 
			<br>
			<label for='newPassword'> New Password: </label> 
			<br>
			<input type='password' name='newPassword' id='newPassword' maxlength="50" /> 
			<br>
			<label for='confirmNewPassword'> Confirm Current Password:</label>
			<br>
			<input type='password' name='confirmNewPassword' id='confirmNewPassword' maxlength="50" /> 
			<input type='submit' name='Submit' value='Submit'/>
		</fieldset>
	</form>
<?php
include "footer.php";
?>