<?php
include 'header.php';

$pageTitle = "Export Data";
if(isset($_SESSION['error']))
{
    echo '<h2>'.$_SESSION['error'].'</h2>';
    unset($_SESSION['error']);
}
?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script lang="javascript" src="xlsx.full.min.js"></script>
<script lang="javascript" src="exportExcel.js"></script>

<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" /> <input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Back to Admin Controls" onclick="window.location.href='admin.php'"<br><br>
<h1> Export Attendance Data to Excel: </h1>
	<fieldset>
	<legend id = "export">Select a Date</legend>
	<form action = '' method = 'post' accept-charset='UTF-8'><input name = 'pickedDate' type='date' id='datepicker'>
	<input type='submit' name='Submit' value='Submit'/>
	</form>
	<?php
if(isset($_POST['pickedDate'])){
    $date = $_POST['pickedDate'];
	$sheets = array("BottomTop","MainLEFT","MainRIGHT");
	echo '<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left;" value="Download" id="button-a"><br>';

	$times = array();
	$names = array();
	$attendance = array(array(), array(), array());
	
	for ($i = 0; $i < 3; $i++) {
		array_push($times, getTimes($pdo, $date, $i));
		array_push($names, getSectionNames($pdo, $i));
	}
	for ($i = 0; $i < 3; $i++) {
		$time = $times[$i];
		foreach($time as $timeTaken) {
			array_push($attendance[$i], getAttendance($pdo, $date, $i, $timeTaken['timeTaken']));
		}
	}
?>
<script>
function main() {
	const sumArr = arr => arr.reduce((x,y) => x + y, 0);
	var sheets = ["BottomTop","MainLEFT","MainRIGHT"];
	var names = <?php echo json_encode($names); ?>;
	var attendance = <?php echo json_encode($attendance); ?>;
	var times = <?php echo json_encode($times); ?>;
	var wb = XLSX.utils.book_new();
		wb.Props = {
			Title: "Attendance <?php echo $date ?>",
			Subject: "Library",
			Author: "Muskie Library Attendance"
		};
	for (var i = 0; i < 3; i++) {
		console.log(times[i].length);
		if (times[i].length != 16) {
			alert("Insufficient data for sheet " + sheets[i] + ".");
			return;
		}
		wb.SheetNames.push(sheets[i]);
		var data = [[]];
		var columnTotals = [];
		var style = [];
		data[0].push("");
		columnTotals.push("");
		style.push({"wch":"7"});
		for(var j = 0; j < names[i].length; j++) {
			data[0].push(names[i][j]['sectionName']);
			style.push({"wch":"3"});
		}
		var totalRows = 0;
		for(var j = 0; j < times[i].length; j++) {
			var rowTotal = 0;
			data[j+1] = [];
			data[j+1].push(times[i][j]['timeTaken']);
			for(var k = 0; k < attendance[i][j].length; k++) {
				var numPeople = parseInt(attendance[i][j][k]['numPeople']);
				data[j+1].push(numPeople);
				rowTotal += parseInt(numPeople);
				if (typeof columnTotals[k+1] === 'undefined') {
					columnTotals[k+1] = 0;
				}
				columnTotals[k+1] += numPeople;
			}
			data[j+1].push("");
			data[j+1].push("");
			data[j+1].push(rowTotal);
			totalRows += rowTotal;
		}
		var totalColumns = [sumArr(columnTotals.slice(1,columnTotals.length))];
		columnTotals.push("", "", totalRows);
		style.push({"wch":"3"},{"wch":"3"},{"wch":"4"});
		data.push(columnTotals);
		for (var j = 0; j < columnTotals.length - 1; j++) {
			totalColumns.unshift("");
		}
		data.push(totalColumns);
		var ws = XLSX.utils.aoa_to_sheet(data);
		ws['!cols'] = style;
		wb.Sheets[sheets[i]] = ws;
	}
	wb.SheetNames.push("Totals");
	wb.Sheets["Totals"] = totalsSheet();

	var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'base64'});

	$("#button-a").click(function(){
		XLSX.writeFile(wb, <?php echo "'Attendance_".$date.".xlsx'" ?>, {type:"base64"});
		});
}
main();
</script>
</fieldset>
<?php
}
?>
<?php
include "footer.php";
?>