<?php
include 'header.php';

$pageTitle = "Create New User";
if(isset($_SESSION['error']))
{
    echo '<h2>'.$_SESSION['error'].'</h2>';
    unset($_SESSION['error']);
}
?>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" /> 
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Back to Admin Controls" onclick="window.location.href='admin.php'"<br><br>
<h1> Create Account: </h1>
	<form id='createAccount' action='createAccount.php' method='post' accept-charset='UTF-8'>
		<fieldset>
			<legend id = "create">Create Account</legend>
			<label for='userID'> User Name: </label> 
			<br>
			<input type='text' name='userName' id='userName' maxlength="100" /> 
			<br>
			<label for='email'> Email: </label>
			<br>
			<input type='text' name='email' id='email' maxlength="100" />
			<br>
			<label for='password'> Password: </label> 
			<br>
			<input type='password' name='password' id='password' maxlength="50" /> 
			<br>
			<label for='confirmPassword'> Confirm Password:</label>
			<br>
			<input type='password' name='confirmPassword' id='confirmPassword' maxlength="50" /> 
			<br>
			User is Admin: <input type='checkbox' name='isAdmin' id='isAdmin' value='Yes'>
			<br>
			<input type='submit' name='Submit' value='Submit'/>
		</fieldset>
	</form>
<?php
include "footer.php";
?>