<?php
include "dbconnect.php";

function createAccount(PDO $pdo, $userName, $email, $pass, $isAdmin)
{
    $password = password_hash($pass, PASSWORD_BCRYPT);
    $sql = 'INSERT INTO users (userName, email, password, isAdmin) VALUES (:userName, :email, :password ,:isAdmin)';
    $stmt = $pdo->prepare($sql);
    $stmt ->bindParam(':userName', $userName);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':password', $password);
	$stmt->bindParam(':isAdmin', $isAdmin);
    $stmt->execute();
    
    $sql = 'SELECT * FROM users WHERE email = :email LIMIT 1';
    $stmt = $pdo->prepare($sql);
	$stmt->bindParam(':email', $email);
    $stmt->execute();
    $row = $stmt->fetch();
    return ($row);
}

function changePassword(PDO $pdo, $userName, $pass, $newPass) {
	if (confirmLogin($pdo, $userName, $pass)) {
		$password = password_hash($newPass, PASSWORD_BCRYPT);
		$sql = 'UPDATE users SET password = :password WHERE userName = :userName';
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':userName', $userName);
		$stmt->bindParam(':password', $password);
		$stmt->execute();
		return true;
	}
	else {
		return false;
	}
}

function confirmLogin(PDO $pdo, $userName, $pass)
{
    $sql = 'SELECT * FROM users WHERE userName = :userName';
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':userName', $userName);
    $stmt->execute();
    $row = $stmt->fetch();
    if (password_verify($pass, $row['password'])) {
        return true;
    } 
    else 
    {
        return false;
    }
}
function userExists(PDO $pdo, $email)
{
    $exist = htmlspecialchars($email, ENT_QUOTES, 'UTF-8');
    $sql = 'SELECT * FROM users WHERE email = :email';
    $s = $pdo->prepare($sql);
    $s->bindValue(':email', $exist);
    $s->execute();
    
    if($s -> rowCount() > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
function getUser(PDO $pdo, $userID)
{
    $sql = 'SELECT * FROM users WHERE userID = :userID';
    $s = $pdo->prepare($sql);
    $s->bindValue(':userID', $userID);
    $s->execute();
    
    if($s -> rowCount() > 0)
    {
        return $s->fetch();
    }
    else
    {
        return false;
    }
}

function getAllFloors(PDO $pdo)
{
    $sql = 'SELECT * FROM floor';
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    return $stmt->fetchAll();
    
}
function getFloorInfo(PDO $pdo, $floorID)
{
    $sql = 'SELECT * FROM floor WHERE floorID = :floorID';
    
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':floorID', $floorID);
    $stmt->execute();
    
    return $stmt->fetchAll();
}
function getCoords(PDO $pdo, $floorID)
{
    $sql = 'SELECT * FROM section WHERE floorID = :floorID';
    
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':floorID', $floorID);
    $stmt->execute();
    
    return $stmt->fetchAll();
}
function takeAttendance(PDO $pdo, $numPeople, $sectionID, $floorID, $timeTaken, $date)
{
    $sql = 'INSERT INTO attendance (numPeople, sectionID, floorID, timeTaken, date) VALUES (:numPeople, :sectionID, :floorID, :timeTaken, :date)';
    $stmt = $pdo->prepare($sql);
    $stmt ->bindParam(':numPeople', $numPeople);
    $stmt->bindParam(':sectionID', $sectionID);
    $stmt->bindParam(':floorID', $floorID);
	$stmt->bindParam(':timeTaken', $timeTaken);
	$stmt->bindParam(':date', $date);
    $stmt->execute();
}
function getAttendance(PDO $pdo, $date, $floor, $time)
{
    $sql = 'SELECT * FROM attendance WHERE date = :date AND timeTaken = :time AND floorID IN (SELECT floorID FROM floor WHERE sheetID = :floor ORDER BY floorID ASC) ORDER BY sectionID ASC';
    
    $stmt = $pdo->prepare($sql);
    $stmt -> bindParam(':date', $date);
	$stmt -> bindParam(':floor', $floor);
	$stmt -> bindParam(':time', $time);
    $stmt->execute();
    
    return $stmt->fetchAll();
} 
function getSectionNames(PDO $pdo, $floor)
{
	$sql = 'SELECT DISTINCT sectionName FROM section WHERE floorID IN (SELECT floorID FROM floor WHERE sheetID = :floor ORDER BY floorID ASC) ORDER BY sectionID ASC';
	
	$stmt = $pdo->prepare($sql);
	$stmt -> bindParam(':floor', $floor);
	$stmt->execute();
	
	return $stmt->fetchAll();
}
function getTimes(PDO $pdo, $date, $floor)
{
	$sql = 'SELECT DISTINCT timeTaken FROM attendance WHERE date = :date AND floorID IN (SELECT floorID FROM floor WHERE sheetID = :floor) ORDER BY timeTaken ASC';
    
    $stmt = $pdo->prepare($sql);
	$stmt -> bindParam(':date', $date);
	$stmt -> bindParam(':floor', $floor);
	$stmt->execute();
	
	return $stmt->fetchAll();
}
?>