<?php
include "header.php";
?>

<?php
if(!isset($_SESSION['key']))
{
    $_SESSION['error'] = 'You must be logged in to view attendance.';
    header("Location: main.php");
    exit();
}
?>
<html>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Return home" onclick="window.location.href='homePage.php'" />
<?php
	$user = getUser($pdo, $_SESSION['key']);
	
	if ($user['isAdmin'])
	{
?>
<input type="button" style = "padding: 8px 16px;color: white; background-color: black;float: left" value="Export Data to Excel" onclick="window.location.href='exportExcel.php'">
<?php
	}
	?>
	<br>
<h1> Library Attendance Data: </h1>
  <!-- OLD DATE PICKER CODE (SAVING IN CASE OF FUTURE NEED)
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>-->
 
<p>Click box to select date:
<form action = 'displayData.php' method = 'post' accept-charset='UTF-8'><input name = 'pickedDate' type='date' id='datepicker'>  
</p>
<p>
Click box to select floor: <select name = "pickedFloor" class = "form">
<option selected value = ""></option>
<option value="1">Main Left</option>
<option value="2">Main Right</option>
<option value="0">Bottom & Top</option>
</select>
<input type = 'submit' name = 'Submit' value = 'Submit'>
</form>
</p>    
<?php 
if (isset($_POST['pickedDate']) && isset($_POST['pickedFloor']))
{
	$date = $_POST['pickedDate'];
    $floor = $_POST['pickedFloor'];
    ?>      	
<style>
    .attendanceTable 
    {
       border-collapse: collapse;
	   font-size: 20 px;
	   min-width: 1000px;
    }

    .attendanceTable th, 
    .attendanceTable td 
    {
		border: 1px solid black;
		padding: 5px 15x;
	}
	
	.attendanceTable caption 
	{
	   margin: 5px;
	}

/*header*/
    .attendanceTable thead th 
    {
		background-color: black;
		color: white;
		border-color: black;
		text-transform: uppercase;
	}

/*table body*/
	.attendanceTable tbody th 
    {
		background-color: black;
		color: white;
		border-color: black;
		text-transform: uppercase;
	}
	.attendanceTable tbody td 
	{
	    background-color: white;
		color: black;
	   text-align: center;
	}
	.attendanceTable tbody tr:hover td
	{
		background-color: yellow;
		border-color: black;
	}
	.attendanceTable tbody tr:hover th
	{
		color: black;
		background-color: yellow;
		border-color: black;
	}
/*footer*/
    .attendanceTable tfoot th 
    {
		background-color: black;
		color: white;
		border-color: black;
		text-transform: uppercase;
	}
	.attendanceTable tfoot th:first-child 
	{
		text-align: left;
	}
	.attendanceTable tbody td:empty
	{
		background-color: grey;
	}
</style>
<?php 
    $allTimes = getTimes($pdo, $date, $floor);
	$sectionNames = getSectionNames($pdo, $floor);
	$totals = array();
	$i = 0;
	
    if(count($allTimes) > 0)
    { ?>
<table class = "attendanceTable" id = "attendanceTable">
	<thead>
		<tr>
			<th></th>
			<?php
			foreach ($sectionNames as $sectionName)
			{ ?>
				<th><?php echo $sectionName['sectionName']?></th>
				<?php
			} ?>
		</tr>
	</thead>
	<tbody>
		<?php 
		  foreach ($allTimes as $displayTime) 
		  { 
			$time = $displayTime['timeTaken'];
		  ?>
		  	<tr>
			<th><?php echo $time?></th>
			<?php
			$allAttendance = getAttendance($pdo, $date, $floor, $time);
			foreach ($allAttendance as $displayAttendance)
			{ 
				$numPeople = $displayAttendance['numPeople'];
			?>
			<td><?php echo $numPeople?></td>
			<?php
			if (!isset($totals[$i])) {
				$totals[$i] = 0;
			}
			$totals[$i] += $numPeople;
			$i++;
			} ?>
			</tr><?php	
			$i = 0;
		   }
		   ?>
		</tbody>
		<tfoot>
			<tr>
				<th "3"></th>
				<?php
				foreach($totals as $totalPeople)
				{
				?>
				<th><?php echo $totalPeople ?></th>
				<?php
				}
    }
    else
    {
        echo "No data found for ". $date. ".";
    } ?></th>
			</tr>
		</tfoot>
	</table> 
<?php 
}
else
{
    echo "Data will be displayed here upon date selection:";
}
?>

<?php 
	include "footer.php";
?>